"use strict"

const request = require("tinyreq");
const cheerio = require("cheerio");

function scrape(url, data, cb) {
    request(url, (err, body) => {
        if (err) { return cb(err); }

        let $ = cheerio.load(body)
            , pageData = {}
            ;

        Object.keys(data).forEach(k => {
            pageData[k] = $(data[k]).text();
        });

        cb(null, pageData);
    });
}

scrape("http://ionicabizau.net/", {
    title: ".header h1",
    description: ".header h2"
}, (err, data) => {
    console.log(err || data);
});
